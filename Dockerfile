FROM python:3

ADD hello.py /

RUN pip install flask

EXPOSE 5000

CMD [ "python", "./hello.py" ]
